﻿namespace tech_test_payment_api.Entities
{
    public class Seller
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string CPF { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
    }
}
