﻿namespace tech_test_payment_api.Entities
{
    public class Sale
    {
        public int Id { get; set; }
        public int IdSeller { get; set; }
        public string SoldItem { get; set; }
        public DateTime Date { get; set; }
        public EnumStatusVenda status { get; set; }


    }
}
