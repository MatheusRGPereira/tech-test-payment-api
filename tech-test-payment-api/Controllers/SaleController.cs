﻿using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;


namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly SalesContext _context;
        private Seller seller;
        private SalesDto _salesDto;

        public SaleController(SalesContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult AddSale(Sale sale)
        {
            var seller = _context.Sellers.Find(sale.IdSeller);
            if (seller == null)
                return NotFound("Vendedor nao encontrado");
            if(string.IsNullOrEmpty(sale.SoldItem))
                return BadRequest("Precisa ao menos um item para realizar a venda");
            sale.status = EnumStatusVenda.Aguardando;
            _context.Sales.Add(sale);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { id = sale.Id }, sale);

        }



        // GET: api/<SaleController>
        [HttpGet]
        public IActionResult GetAllSales()
        {
            var sales = _context.Sales.ToList();
            SalesDto salesDto;

            List<SalesDto> salesList = new List<SalesDto>();

            foreach(var sale in sales)
            {
                salesDto = new SalesDto();
                salesDto.AddSale(sale);
                salesDto.AddSeller(Validator.GetSeller(sale.IdSeller, _context));
                salesList.Add(salesDto);   
            }
            return Ok(salesList);

        }

        // GET api/<SaleController>/5
        [HttpGet("ObterVendaPorId/{id}")]
        public IActionResult GetById(int id)
        {
            var sale = _context.Sales.Find(id);
            if (sale == null) return NotFound("Objeto não encontrado");
            SalesDto salesDto;
            salesDto = new SalesDto();
            salesDto.AddSale(sale);
            salesDto.AddSeller(Validator.GetSeller(sale.IdSeller, _context));
            return Ok(salesDto);
        }

        [HttpPut("AtualizarStatus/{id}")]
        public IActionResult StatusUpdate(int id, Sale sale)
        {
            var saleVerify = _context.Sales.Find(id);
            if (sale == null) return NotFound("Objeto não encontrado");
            if(Validator.Update(sale, saleVerify))
            {
                saleVerify.status = sale.status;
                _context.SaveChanges();
                return Ok(saleVerify);
            }
            if (saleVerify.status == EnumStatusVenda.Aguardando) return UnprocessableEntity("A venda já foi entregue");
            if (saleVerify.status == EnumStatusVenda.Cancelado) return UnprocessableEntity("A venda foi cancelada");
            return UnprocessableEntity("Caiu aqui");
        }

   
    }

    public static class Validator
    {
        public static Seller GetSeller(int id, SalesContext salesContext)
        {
            var seller = salesContext.Sellers.Find(id);
            return seller;
        }


        public static bool Update(Sale sale, Sale saleVerify)
        {
            if (saleVerify.status.Equals(EnumStatusVenda.Aguardando))
            {
                if (sale.status.Equals(EnumStatusVenda.Aprovado) || sale.status.Equals(EnumStatusVenda.Cancelado))
                    {
                    return true;
                }
            }
            if (saleVerify.status.Equals(EnumStatusVenda.Aprovado))
            {
                if (sale.status.Equals(EnumStatusVenda.EnviadoParaTransportadora) || sale.status.Equals(EnumStatusVenda.Cancelado))
                    {
                    return true;
                }
            }
            if (saleVerify.status.Equals(EnumStatusVenda.EnviadoParaTransportadora))
            {
                if (sale.status.Equals(EnumStatusVenda.Aprovado))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
