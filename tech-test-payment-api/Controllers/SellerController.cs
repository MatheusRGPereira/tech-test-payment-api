﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly SalesContext _context;

        public SellerController(SalesContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult AddSeller(Seller seller)
        {
            _context.Add(seller);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { id = seller.Id }, seller);
        }

        [HttpGet("ObterTodosVendedores")]
        public IActionResult GetAllSellers()
        {
            var sellers = _context.Sellers.ToList();
            return Ok(sellers);       
        }

        [HttpGet("ObterPorId/{id}")]
        public IActionResult GetById(int id)
        {
            var seller = _context.Sellers.Find(id);
            if (seller == null) return NotFound("Objeto Não encontrado");
            return Ok(seller);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, Seller seller)
        {
            var sellerVerify = _context.Sellers.Find(id);
            if (id == null) return NotFound("Objeto não encontrado");
            sellerVerify.Name = seller.Name;
            sellerVerify.Telephone = seller.Telephone;
            sellerVerify.CPF = seller.CPF;
            sellerVerify.Email = seller.Email;
            _context.Sellers.Update(sellerVerify);
            _context.SaveChanges();
            return Ok(sellerVerify);
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteById(int id)
        {
            var sellerVerify = _context.Sellers.Find(id);
            if (sellerVerify == null) return NotFound("Objeto Não Encontrado");
            _context.Sellers.Remove(sellerVerify);
            _context.SaveChanges();
            return NoContent();
        }
     }
}
