﻿using tech_test_payment_api.Entities;

namespace tech_test_payment_api
{
    public class SalesDto
    {
        public int IdSale { get; set; }
        public DateTime Date { get; set; }

        public string soldItem { get; set; }

        public EnumStatusVenda status { get; set; }



        public int IdSeller { get; set; }
        public string Name { get; set; }
        public string CPF { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }

        public void AddSale(Sale sale)
        {
            IdSale = sale.Id;
            Date = sale.Date;
            soldItem = sale.SoldItem;
            status = sale.status;
        }

        public void AddSeller(Seller seller)
        {
            IdSeller = seller.Id;
            CPF = seller.CPF;
            Name = seller.Name;
            Telephone = seller.Telephone;
            Email = seller.Email;
        }

    }
}
